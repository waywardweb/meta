# Welcome to the Wayward Web!

The Wayward Web project aims to curate, catalog, and promote websites that are made for humans, by humans.

- [About our goals](about/goals.html).
- [Who we are](about/us.html).

## Big List O' Links

### Other Web Directories

- [Ytoo.org](https://ytoo.org) - list of '90s and 2000s-themed sites, including recreations of early social networks (Facebook, MySpace, YouTube) and games (NeoPets).
- [Explore Nekoweb.org](https://nekoweb.org/explore) - directory of the social static site host [Nekoweb](https://nekoweb.org).
- [Browse Neocities.org](https://neocities.org/browse) - directory of the social static site host [Neocities](https://neocities.org).
- [Bastion](https://bastionhome.github.io) - Ben Christel's personal list of most-used sites.

### Web Archives

- [Archive.org](https://archive.org) - a.k.a. the Wayback Machine. Snapshots of almost every site on the web going back 30+ years.
- [Ghost Archive](https://ghostarchive.org/) - Archives of various web media including videos.